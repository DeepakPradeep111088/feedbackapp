package com.bestdocs.feedbackapp.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bestdocs.feedbackapp.R
import com.bestdocs.feedbackapp.data.model.PatientData
import com.mindorks.retrofit.coroutines.utils.showcommentsDialog
import kotlinx.android.synthetic.main.item_layout.view.*

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
class MainAdapter(private val users: ArrayList<PatientData>) : RecyclerView.Adapter<MainAdapter.DataViewHolder>() {

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(user: PatientData) {
            itemView.apply {
                patientNamee.text ="Patient Name  : " + user.patientName
                mobilenum.text    ="Patient Phone : " +user.patientmobile
                ratingg.text      ="Rating              : " +user.ratingvalue

                if(user.comments?.length == 0 && user.feedbacklist?.size==0)
                {
                    comments.visibility = View.GONE
                }

                comments.setOnClickListener {
                    val dialogg = showcommentsDialog(context,user.comments,user.feedbacklist)
                    dialogg.show()
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder =
        DataViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false))

    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bind(users[position])
    }

    fun addUsers(users: List<PatientData>) {
        this.users.apply {
            clear()
            addAll(users)
        }

    }
}