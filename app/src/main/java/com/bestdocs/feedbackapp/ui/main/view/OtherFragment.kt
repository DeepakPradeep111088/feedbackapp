package com.bestdocs.feedbackapp.ui.main.view

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bestdocs.feedbackapp.R
import com.mindorks.retrofit.coroutines.utils.StarRatingDialog
import kotlinx.android.synthetic.main.fragment_others.*

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
class OtherFragment() : Fragment() {

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_others, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        comments.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                StarRatingDialog.dataToSave.comments = s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

}