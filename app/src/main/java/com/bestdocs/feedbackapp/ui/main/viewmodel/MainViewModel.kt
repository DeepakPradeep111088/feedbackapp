package com.bestdocs.feedbackapp.ui.main.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.bestdocs.feedbackapp.data.model.FeedbackResponse
import com.bestdocs.feedbackapp.data.model.PatientData
import com.bestdocs.feedbackapp.data.repository.MainRepository
import com.mindorks.retrofit.coroutines.utils.Resource
import kotlinx.coroutines.Dispatchers

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
class  MainViewModel(private val mainRepository: MainRepository) : ViewModel() {

    var liveDataFeedbackOptions: LiveData<FeedbackResponse>? = null

    fun getUsers() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getUsers()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun insertData(context: Context, feed: FeedbackResponse) {
        mainRepository.insertData(context, feed)
    }


    fun getFeedBackOptions(context: Context) : LiveData<FeedbackResponse>? {
        liveDataFeedbackOptions = mainRepository.getFeedBackOptions(context)
        return liveDataFeedbackOptions
    }


    fun insertFeedback(context: Context, feed: PatientData) {
        mainRepository.insertPatientFeedback(context, feed)
    }

}