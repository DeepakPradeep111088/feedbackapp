package com.bestdocs.feedbackapp.ui.main.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bestdocs.feedbackapp.R
import com.bestdocs.feedbackapp.data.model.PatientData
import com.bestdocs.feedbackapp.ui.main.adapter.MainAdapter
import com.bestdocs.feedbackapp.ui.main.viewmodel.ListingViewModel
import com.bestdocs.feedbackapp.ui.main.viewmodel.ListingViewModelFactory
import com.bestdocs.feedbackapp.utils.CustomProgressDialog
import com.mindorks.retrofit.coroutines.utils.Status
import kotlinx.android.synthetic.main.activity_listing.*

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
class ListingActivity : AppCompatActivity() {

    private lateinit var viewModel: ListingViewModel
    lateinit var adapter: MainAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listing)
        setupViewModel()
        setupUI()
        setupObservers()
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ListingViewModelFactory()
        ).get(ListingViewModel::class.java)
    }


    private fun setupUI() {
        recyclerView.layoutManager = LinearLayoutManager(this@ListingActivity)
        adapter = MainAdapter(arrayListOf())
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                (recyclerView.layoutManager as LinearLayoutManager).orientation
            )
        )
        recyclerView.adapter = adapter
    }


    private fun setupObservers() {
        viewModel.getAllFeedbacks(this).observe(this, Observer {

            CustomProgressDialog.hideLoadingIcon()
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let { users -> retrieveList(users) }
                    }
                    Status.ERROR -> {
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        CustomProgressDialog.getLoadingIcon(this@ListingActivity, false, false)
                    }
                }
            }
        })
    }

    private fun retrieveList(feeds: List<PatientData>) {
        if (feeds?.size > 0) {
            recyclerView.visibility = View.VISIBLE
            nodatText.visibility = View.GONE
            adapter.apply {
                addUsers(feeds)
                notifyDataSetChanged()
            }
        } else {
            recyclerView.visibility = View.GONE
            nodatText.visibility = View.VISIBLE
        }


    }

}