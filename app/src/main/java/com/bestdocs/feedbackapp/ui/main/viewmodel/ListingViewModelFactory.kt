package com.bestdocs.feedbackapp.ui.main.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bestdocs.feedbackapp.data.repository.ListingRepository

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
class ListingViewModelFactory() : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ListingViewModel::class.java)) {
            return ListingViewModel(ListingRepository()) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}