package com.bestdocs.feedbackapp.ui.main.view

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.bestdocs.feedbackapp.R
import com.bestdocs.feedbackapp.utils.CustomProgressDialog
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
class SplashActivity : AppCompatActivity() {

    val EXTRA_LOCATION = "location"

    var PERMISSION_ID = 101
    var mFusedLocationClient: FusedLocationProviderClient? = null

    var activitycalled = false

    var currentlatitude: Double = 0.0
    var currentlongitude: Double = 0.0

    var locationString :String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mFusedLocationClient = LocationServices
            .getFusedLocationProviderClient(this)

        getLastLocation()
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        // check if permissions are given
        if (checkPermissions()) {
            // check if location is enabled
            if (isLocationEnabled()) {
                mFusedLocationClient
                    ?.getLastLocation()
                    ?.addOnCompleteListener(
                        object : OnCompleteListener<Location?> {
                            override fun onComplete(
                                task: Task<Location?>
                            ) {
                                val location: Location? = task.getResult()
                                if (location == null) {
                                    requestNewLocationData()
                                } else {
                                    currentlatitude = location.latitude
                                    currentlongitude = location.longitude

                                    locationString = String.format(
                                        "%.7f",
                                        currentlatitude
                                    ) + " , " + String.format("%.7f", currentlongitude)


                                    callnextscreen()

                                }
                            }
                        })

            } else {
                Toast
                    .makeText(
                        this, "Please turn on"
                                + " your location...",
                        Toast.LENGTH_LONG
                    )
                    .show()
                val intent = Intent(
                    Settings.ACTION_LOCATION_SOURCE_SETTINGS
                )
                startActivity(intent)
            }
        } else {
            // if permissions aren't available,
            // request for permissions
            requestPermissions()
        }
    }

    private fun checkPermissions(): Boolean {
        return (ActivityCompat
            .checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
                == PackageManager.PERMISSION_GRANTED
                && ActivityCompat
            .checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
                == PackageManager.PERMISSION_GRANTED)
    }

    // method to requestfor permissions
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this, arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            PERMISSION_ID
        )
    }


    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 5
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient?.requestLocationUpdates(
            mLocationRequest,
            mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(
            locationResult: LocationResult
        ) {
            currentlatitude = locationResult.lastLocation.latitude
            currentlongitude = locationResult.lastLocation.longitude
            locationString = String.format(
                "%.7f",
                currentlatitude
            ) + " , " + String.format("%.7f", currentlongitude)


            callnextscreen()

        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == PERMISSION_ID) {
            if (grantResults.size > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                if (isLocationEnabled()) {

                    mFusedLocationClient
                        ?.getLastLocation()
                        ?.addOnCompleteListener(
                            object : OnCompleteListener<Location?> {
                                override fun onComplete(
                                    task: Task<Location?>
                                ) {
                                    val location: Location? = task.getResult()
                                    if (location == null) {
                                        requestNewLocationData()
                                    } else {
                                        currentlatitude = location.latitude
                                        currentlongitude = location.longitude
                                        locationString = String.format(
                                            "%.7f",
                                            currentlatitude
                                        ) + " , " + String.format("%.7f", currentlongitude)


                                        callnextscreen()
                                    }
                                }
                            })

                } else {
                    Toast
                        .makeText(
                            this, "Please turn on"
                                    + " your location...",
                            Toast.LENGTH_LONG
                        )
                        .show()
                    val intent = Intent(
                        Settings.ACTION_LOCATION_SOURCE_SETTINGS
                    )
                    startActivityForResult(intent, 111)
                }
            }

            else
            {
                finish()
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (checkPermissions()) {
            // check if location is enabled
            if (isLocationEnabled()) {

                mFusedLocationClient
                    ?.getLastLocation()
                    ?.addOnCompleteListener(
                        object : OnCompleteListener<Location?> {
                            override fun onComplete(
                                task: Task<Location?>
                            ) {
                                val location: Location? = task.getResult()
                                if (location == null) {
                                    requestNewLocationData()
                                } else {
                                    currentlatitude = location.latitude
                                    currentlongitude = location.longitude
                                    locationString = String.format(
                                        "%.7f",
                                        currentlatitude
                                    ) + " , " + String.format("%.7f", currentlongitude)



                                    callnextscreen()
                                }
                            }
                        })

            } else {
                Toast
                    .makeText(
                        this, "Please turn on"
                                + " your location...",
                        Toast.LENGTH_LONG
                    )
                    .show()
                val intent = Intent(
                    Settings.ACTION_LOCATION_SOURCE_SETTINGS
                )
                startActivity(intent)
            }
        } else {
            // if permissions aren't available,
            // request for permissions
            requestPermissions()
        }
    }

    // method to check
    // if location is enabled
    private fun isLocationEnabled(): Boolean {
        val locationManager = getSystemService(
            Context.LOCATION_SERVICE
        ) as LocationManager
        return (locationManager
            .isProviderEnabled(
                LocationManager.GPS_PROVIDER
            )
                || locationManager
            .isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
            ))
    }

    fun callnextscreen()
    {
        if(!activitycalled) {
            activitycalled = true
            CustomProgressDialog.getLoadingIcon(this@SplashActivity,false,false)

            Handler().postDelayed(Runnable {
                CustomProgressDialog.hideLoadingIcon()
                val intent = Intent(this, MainActivity::class.java).apply {
                    putExtra(EXTRA_LOCATION, locationString)
                    finish()

                }
                startActivity(intent)
            }, 3000)
        }

    }

}