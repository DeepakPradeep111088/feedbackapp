package com.bestdocs.feedbackapp.ui.main.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.bestdocs.feedbackapp.data.repository.ListingRepository
import com.mindorks.retrofit.coroutines.utils.Resource
import kotlinx.coroutines.Dispatchers

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
class ListingViewModel(private val mainRepository: ListingRepository) : ViewModel() {

    fun getAllFeedbacks(context: Context) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getAllFeedbacks(context)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

}