package com.bestdocs.feedbackapp.ui.main.view

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.bestdocs.feedbackapp.R
import com.bestdocs.feedbackapp.data.model.FeedbackResponse
import com.bestdocs.feedbackapp.ui.main.viewmodel.MainViewModel
import com.bestdocs.feedbackapp.utils.CustomProgressDialog
import com.mindorks.retrofit.coroutines.utils.StarRatingDialog
import com.mindorks.retrofit.coroutines.utils.Status
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel<MainViewModel>()
    val EXTRA_LOCATION = "location"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupObservers()

        val locationreceived = intent.getStringExtra(EXTRA_LOCATION)
        locationvalue.setText(locationreceived)

        openfeedbacks.setOnClickListener {
            val intent = Intent(this, ListingActivity::class.java)
            startActivity(intent)
        }


        savetoDb.setOnClickListener {
            if(checkfields()) {
                var dialog =  StarRatingDialog.newInstance(
                    patientName.text.toString(),
                    mobilenumber.text.toString(),viewModel
                )
                dialog.isCancelable = false
                dialog.show(supportFragmentManager, StarRatingDialog.TAG)
                patientName.text = null
                mobilenumber.text= null
            }
            else{
                Toast.makeText(this@MainActivity,"Please fill All the fields", Toast.LENGTH_SHORT).show()
            }

        }
    }


    private fun checkfields(): Boolean {
        var validationcorrect:Boolean = false

        if(patientName.length()>0
            && mobilenumber.length()>0)
        {
            validationcorrect = true
        }

        return validationcorrect
    }

    private fun setupObservers() {

        CustomProgressDialog.getLoadingIcon(this@MainActivity,false,false)

        viewModel.getFeedBackOptions(this)?.observeOnce(this, Observer<FeedbackResponse> {

            CustomProgressDialog.hideLoadingIcon()
            if(it?.feedback == null)
            {
                viewModel.getUsers().observe(this, Observer {
                    it?.let { resource ->
                        when (resource.status) {
                            Status.SUCCESS -> {
                                CustomProgressDialog.hideLoadingIcon()
                                resource.data?.let { feedbackResponse -> retrieveList(feedbackResponse) }
                            }
                            Status.ERROR -> {
                                Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                            }
                            Status.LOADING -> {
                                CustomProgressDialog.getLoadingIcon(this@MainActivity,false,false)
                            }
                        }
                    }
                })


            }
        })

    }

    private fun retrieveList(data: FeedbackResponse) {
        viewModel.insertData(this@MainActivity, data)
    }

    fun <T> LiveData<T>.observeOnce(lifecycleOwner: LifecycleOwner, observer: Observer<T>) {
        observe(lifecycleOwner, object : Observer<T> {
            override fun onChanged(t: T?) {
                observer.onChanged(t)
                removeObserver(this)
            }
        })
    }
}