package com.bestdocs.feedbackapp.ui.main.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bestdocs.feedbackapp.R
import com.bestdocs.feedbackapp.data.model.Feedback
import com.bestdocs.feedbackapp.data.model.FeedbackOptiontosave
import com.bestdocs.feedbackapp.data.model.FeedbackResponse
import com.bestdocs.feedbackapp.ui.main.viewmodel.MainViewModel
import com.mindorks.retrofit.coroutines.utils.StarRatingDialog
import kotlinx.android.synthetic.main.fragment_audio.*

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
class VideoFragment() : Fragment() {

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_video, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()

        viewModel.getFeedBackOptions(requireContext())?.observe(viewLifecycleOwner, Observer {
            it?.let { resource ->
                if(it?.feedback != null)
                {
                    retrieveList(it)
                }
            }
        })



    }

    private fun setupViewModel() {
        viewModel = StarRatingDialog.viewModel
    }


    @SuppressLint("NewApi")
    private fun retrieveList(feedrep: FeedbackResponse) {
//        Toast.makeText(activity,"retrieved", Toast.LENGTH_SHORT).show()


        var feedback: Feedback? = null
        for(feed in feedrep.feedback)
        {
            if(feed.optionName == "Video")
            {
                feedback = feed
            }
        }

        for(option in feedback?.feedbackOptions!!){
            var checkBox = CheckBox(activity)
            checkBox.text = option.name
            checkBox.textSize = 20F
            checkBox.id = option.id
            checkBox.setButtonTintList(
                AppCompatResources.getColorStateList(
                    requireContext(),
                    R.color.colorCheckbox
                )
            )


            checkBox.setOnClickListener( View.OnClickListener {

                if(StarRatingDialog.dataToSave.feedbacklist.isNullOrEmpty())
                {
                    var feed: FeedbackOptiontosave = FeedbackOptiontosave()
                    feed.id = option.id
                    feed.name = option.name

                    StarRatingDialog.dataToSave.feedbacklist.add(feed)
                }
                else
                {
                    var feedbackOptionList:ArrayList<FeedbackOptiontosave> =  StarRatingDialog.dataToSave.feedbacklist
                    var itemRemoved = false
                    for(item in feedbackOptionList)
                    {
                        if(item.id == option.id)
                        {
                            itemRemoved = true
                            StarRatingDialog.dataToSave.feedbacklist.remove(item)
                            break
                        }
                    }

                    if(!itemRemoved)
                    {
                        var feed:FeedbackOptiontosave = FeedbackOptiontosave()
                        feed.id = option.id
                        feed.name = option.name
                        StarRatingDialog.dataToSave.feedbacklist.add(feed)
                    }
                }

            })

            main_container.addView(checkBox)

        }
    }

}