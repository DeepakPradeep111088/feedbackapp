package com.bestdocs.feedbackapp.data.model

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
class Converters {

        @TypeConverter
        open fun fromFeedbackList(optionValues: ArrayList<FeedbackOptiontosave>): String? {
            if (optionValues == null) {
                return null
            }
            val gson = Gson()
            val type =
                object : TypeToken<ArrayList<FeedbackOptiontosave>>() {}.type
            return gson.toJson(optionValues, type)
        }

        @TypeConverter // note this annotation
        fun toFeedbackList(feedString: String?): ArrayList<FeedbackOptiontosave>? {
            if (feedString == null) {
                return null
            }
            val gson = Gson()
            val type =
                object : TypeToken<ArrayList<FeedbackOptiontosave>>() {}.type
            return gson.fromJson<ArrayList<FeedbackOptiontosave>>(feedString, type)

    }



}