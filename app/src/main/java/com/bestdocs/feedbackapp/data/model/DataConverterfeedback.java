package com.bestdocs.feedbackapp.data.model;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class DataConverterfeedback implements Serializable {

        @TypeConverter // note this annotation
        public String fromFeedbackList(List<Feedback> optionValues) {
            if (optionValues == null) {
                return (null);
            }
            Gson gson = new Gson();
            Type type = new TypeToken<List<Feedback>>() {
            }.getType();
            String json = gson.toJson(optionValues, type);
            return json;
        }

        @TypeConverter // note this annotation
        public List<Feedback> toFeedbackList(String feedString) {
            if (feedString == null) {
                return (null);
            }
            Gson gson = new Gson();
            Type type = new TypeToken<List<Feedback>>() {
            }.getType();
            List<Feedback> productCategoriesList = gson.fromJson(feedString, type);
            return productCategoriesList;
        }


}
