package com.bestdocs.feedbackapp.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "Feedback")
public class FeedbackResponse implements Serializable
{

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public Integer Id = null;

    @TypeConverters(DataConverterfeedback.class)
    @SerializedName("feedback")
    @Expose
    private List<Feedback> feedback = null;
    @SerializedName("isSubmitted")
    @Expose
    private Boolean isSubmitted;
    private final static long serialVersionUID = -8699054959189683995L;

    public List<Feedback> getFeedback() {
        return feedback;
    }

    public void setFeedback(List<Feedback> feedback) {
        this.feedback = feedback;
    }

    public Boolean getIsSubmitted() {
        return isSubmitted;
    }

    public void setIsSubmitted(Boolean isSubmitted) {
        this.isSubmitted = isSubmitted;
    }

}
