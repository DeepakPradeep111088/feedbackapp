package com.bestdocs.feedbackapp.data.repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.bestdocs.feedbackapp.data.api.ApiService
import com.bestdocs.feedbackapp.data.model.FeedbackResponse
import com.bestdocs.feedbackapp.data.model.PatientData
import com.bestdocs.feedbackapp.room.PatientDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainRepository(private val apiService: ApiService) {

    var loginDatabase: PatientDatabase? = null

    var loginTableModel: LiveData<FeedbackResponse>? = null

    var patientData: LiveData<PatientData>? = null

    suspend fun getUsers() = apiService.getUsers(1650)

    fun insertData(context: Context, feedbackOption: FeedbackResponse) {

        loginDatabase = initializeDB(context)

        CoroutineScope(Dispatchers.IO).launch {
            loginDatabase!!.feedbackDao().InsertData(feedbackOption)
        }

    }

    fun insertPatientFeedback(context: Context, patientdata: PatientData) {

        loginDatabase = initializeDB(context)

        CoroutineScope(Dispatchers.IO).launch {
            loginDatabase!!.patientDao().InsertFeedback(patientdata)
        }

    }

    fun initializeDB(context: Context) : PatientDatabase {
        return PatientDatabase.getDataseClient(context)
    }

    fun getFeedBackOptions(context: Context) : LiveData<FeedbackResponse>? {

        loginDatabase = initializeDB(context)

        loginTableModel = loginDatabase!!.feedbackDao().getLoginDetails()

        return loginTableModel
    }

}