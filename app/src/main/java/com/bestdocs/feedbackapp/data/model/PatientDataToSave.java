package com.bestdocs.feedbackapp.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
@Entity
public class PatientDataToSave {

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "patientId")
    private Integer patientId;

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientmobile() {
        return patientmobile;
    }

    public void setPatientmobile(String patientmobile) {
        this.patientmobile = patientmobile;
    }

    public String getPatientlocation() {
        return patientlocation;
    }

    public void setPatientlocation(String patientlocation) {
        this.patientlocation = patientlocation;
    }

    public List<Feedback> getFeedback() {
        return feedback;
    }

    public void setFeedback(List<Feedback> feedback) {
        this.feedback = feedback;
    }

    @SerializedName("patientName")
    @Expose
    private String patientName;

    @SerializedName("patientmobile")
    @Expose
    private String patientmobile;


    @SerializedName("patientlocation")
    @Expose
    private String patientlocation;


    @TypeConverters(DataConverterfeedback.class)
    @SerializedName("feedback")
    @Expose
    private List<Feedback> feedback = null;

}
