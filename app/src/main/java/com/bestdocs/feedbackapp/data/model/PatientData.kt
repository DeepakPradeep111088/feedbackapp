package com.bestdocs.feedbackapp.data.model

import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
@Entity(tableName = "PatientData")
data class PatientData(

    @PrimaryKey()
    var id: Long = 1L,

    @ColumnInfo(name = "patientName")
    var patientName:String,

    @ColumnInfo(name = "patientmobile")
    var patientmobile:String,

    @ColumnInfo(name = "comments")
    var comments:String,

    @ColumnInfo(name = "ratingvalue")
    var ratingvalue:Int,

    @TypeConverters(Converters::class)
    @ColumnInfo(name = "feedbackoptions")
    var feedbacklist:ArrayList<FeedbackOptiontosave>

) {


}