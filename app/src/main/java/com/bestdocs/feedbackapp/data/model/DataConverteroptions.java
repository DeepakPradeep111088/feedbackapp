package com.bestdocs.feedbackapp.data.model;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class DataConverteroptions implements Serializable {


        @TypeConverter // note this annotation
        public String fromFeedOptionsList(List<FeedbackOption> optionValues) {
            if (optionValues == null) {
                return (null);
            }
            Gson gson = new Gson();
            Type type = new TypeToken<List<FeedbackOption>>() {
            }.getType();
            String json = gson.toJson(optionValues, type);
            return json;
        }

        @TypeConverter // note this annotation
        public List<FeedbackOption> toFeedOptionsList(String optionValuesString) {
            if (optionValuesString == null) {
                return (null);
            }
            Gson gson = new Gson();
            Type type = new TypeToken<List<FeedbackOption>>() {
            }.getType();
            List<FeedbackOption> productCategoriesList = gson.fromJson(optionValuesString, type);
            return productCategoriesList;
        }


}
