package com.bestdocs.feedbackapp.data.repository

import android.content.Context
import com.bestdocs.feedbackapp.data.model.PatientData
import com.bestdocs.feedbackapp.room.PatientDatabase

class ListingRepository() {

    var patientdb: PatientDatabase? = null

    var patientData: List<PatientData>? = null


    fun initializeDB(context: Context) : PatientDatabase {
        return PatientDatabase.getDataseClient(context)
    }

    fun getAllFeedbacks(context: Context) : List<PatientData>? {

        patientdb = initializeDB(context)

        patientData = patientdb!!.patientDao().getallFeedback()

        return patientData
    }
}