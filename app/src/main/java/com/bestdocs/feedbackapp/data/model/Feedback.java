
package com.bestdocs.feedbackapp.data.model;

import androidx.room.Entity;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Feedback implements Serializable
{

    @SerializedName("optionId")
    @Expose
    private Integer optionId;
    @SerializedName("optionName")
    @Expose
    private String optionName;
    @TypeConverters(DataConverteroptions.class)
    @SerializedName("feedbackOptions")
    @Expose
    private List<FeedbackOption> feedbackOptions = null;
    private final static long serialVersionUID = 7181149427761536717L;

    public Integer getOptionId() {
        return optionId;
    }

    public void setOptionId(Integer optionId) {
        this.optionId = optionId;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public List<FeedbackOption> getFeedbackOptions() {
        return feedbackOptions;
    }

    public void setFeedbackOptions(List<FeedbackOption> feedbackOptions) {
        this.feedbackOptions = feedbackOptions;
    }

}
