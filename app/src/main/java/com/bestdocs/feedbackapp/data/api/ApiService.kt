package com.bestdocs.feedbackapp.data.api

import com.bestdocs.feedbackapp.data.model.FeedbackResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("feedback")
    suspend fun getUsers(@Query("bookingId") bookId: Int?): FeedbackResponse
}