package com.bestdocs.feedbackapp.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.bestdocs.feedbackapp.data.model.*

@Database(entities = arrayOf(FeedbackResponse::class, PatientData::class), version = 1, exportSchema = false)
@TypeConverters(DataConverterfeedback::class, Converters::class, DataConverteroptions::class)
abstract class PatientDatabase : RoomDatabase() {

    abstract fun feedbackDao() : DAOAccess
    abstract fun patientDao() : PatientDaoAccess

    companion object {

        @Volatile
        private var INSTANCE: PatientDatabase? = null

        fun getDataseClient(context: Context) : PatientDatabase {

            if (INSTANCE != null) return INSTANCE!!

            synchronized(this) {

                INSTANCE = Room
                    .databaseBuilder(context, PatientDatabase::class.java, "PATIENT_FEEDBACK_DB")
                    .fallbackToDestructiveMigration()
                    .build()

                return INSTANCE!!

            }
        }

    }

}