package com.bestdocs.feedbackapp.room

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bestdocs.feedbackapp.data.model.PatientData

@Dao
interface PatientDaoAccess {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun InsertFeedback(loginTableModel: PatientData)

    @androidx.room.Query("SELECT * FROM PatientData")
    fun getallFeedback() : List<PatientData>

}