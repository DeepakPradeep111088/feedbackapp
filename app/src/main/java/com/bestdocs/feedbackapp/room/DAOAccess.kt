package com.bestdocs.feedbackapp.room

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bestdocs.feedbackapp.data.model.FeedbackResponse

@Dao
interface DAOAccess {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun InsertData(loginTableModel: FeedbackResponse)

    @androidx.room.Query("SELECT * FROM Feedback")
    fun getLoginDetails() : LiveData<FeedbackResponse>

}