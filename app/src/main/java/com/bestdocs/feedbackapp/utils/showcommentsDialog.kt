package com.mindorks.retrofit.coroutines.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.bestdocs.feedbackapp.R
import com.bestdocs.feedbackapp.data.model.FeedbackOptiontosave
import kotlinx.android.synthetic.main.comments_dialog.*

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
class showcommentsDialog : Dialog {
    private var mContext: Context
    lateinit var comments:String
    lateinit var feedbacklist:ArrayList<FeedbackOptiontosave>

    constructor(context: Context,comment:String,feedbacklit:ArrayList<FeedbackOptiontosave>) : super(context) {
        mContext = context
        comments = comment
        feedbacklist = feedbacklit
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setContentView(R.layout.comments_dialog)
        if(comments?.length>0)
        {
            var textbox = TextView(mContext)
            textbox.setText("* " +comments)
            textbox.textSize = 18f
            textbox.setTextColor(Color.BLACK)
            val params: LinearLayout.LayoutParams =
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            params.setMargins(10, 10, 10, 10)
            textbox.setLayoutParams(params)
            main_container.addView(textbox)
        }

        for(feeds in feedbacklist)
        {
            var textbox = TextView(mContext)
            textbox.setText("* " + feeds.name)
            textbox.textSize = 18f
            textbox.setTextColor(Color.BLACK)
            val params: LinearLayout.LayoutParams =
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            params.setMargins(10, 10, 10, 10)
            textbox.setLayoutParams(params)

            main_container.addView(textbox)
        }


        submitbutton.setOnClickListener(View.OnClickListener { dismiss() })
    }

    public override fun onStart() {
        super.onStart()
        val param = this.window!!.attributes
        val displayMetrics =
            mContext.resources.displayMetrics
        param.width = displayMetrics.widthPixels
        param?.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        this.window!!.attributes = param
    }
}