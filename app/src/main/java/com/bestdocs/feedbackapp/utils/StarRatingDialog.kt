package com.mindorks.retrofit.coroutines.utils

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.bestdocs.feedbackapp.R
import com.bestdocs.feedbackapp.data.model.FeedbackOptiontosave
import com.bestdocs.feedbackapp.data.model.PatientData
import com.bestdocs.feedbackapp.ui.main.viewmodel.MainViewModel
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.rating_screen.*

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
class StarRatingDialog : DialogFragment() {
    companion object {

        public lateinit var viewModel: MainViewModel

        lateinit var patientName:String
        lateinit var patientMobile:String
        var ratingvalue:Int = 0

        var feedback:ArrayList<FeedbackOptiontosave> = ArrayList<FeedbackOptiontosave>()
        lateinit var dataToSave: PatientData

        const val TAG = "SimpleDialog"

        private const val PATIENT_NAME = "name"
        private const val PATIENT_MOBILE = "mobile"

        fun newInstance(title: String, subTitle: String,viwModel: MainViewModel): StarRatingDialog {
            val args = Bundle()
            args.putString(PATIENT_NAME, title)
            args.putString(PATIENT_MOBILE, subTitle)
            viewModel = viwModel
            val fragment = StarRatingDialog()
            fragment.arguments = args
            return fragment
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        patientName = requireArguments().getString(PATIENT_NAME).toString()
        patientMobile = requireArguments().getString(PATIENT_MOBILE).toString()

        dataToSave = PatientData(1L,patientName, patientMobile,"",0,feedback)
        return inflater.inflate(R.layout.rating_screen, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dataToSave.feedbacklist = arrayListOf()

        tabLayout.addTab(tabLayout.newTab().setText("Audio"))
        tabLayout.addTab(tabLayout.newTab().setText("Video"))
        tabLayout.addTab(tabLayout.newTab().setText("Others"))
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        val adapter = MyAdapter(childFragmentManager,tabLayout.tabCount)

        skippp.setOnClickListener {
            when (tabLayout.selectedTabPosition) {
                0 -> {
                    skippp.visibility = View.VISIBLE
                    submitt.visibility = View.VISIBLE
                    val tab = tabLayout.getTabAt(1)
                    tab!!.select()
                }
                1 -> {
                    skippp.visibility = View.GONE
                    submitt.visibility = View.VISIBLE
                    val tab = tabLayout.getTabAt(2)
                    tab!!.select()
                }
            }
           }

            close.setOnClickListener { dialog?.dismiss() }

//            val stars = ratingBar.progressDrawable as LayerDrawable
//            stars.getDrawable(2).setColorFilter(Color.parseColor("#F69505"), PorterDuff.Mode.SRC_ATOP)
//            stars.getDrawable(0).setColorFilter(Color.parseColor("#F69505"), PorterDuff.Mode.SRC_ATOP)
//            stars.getDrawable(1).setColorFilter(Color.parseColor("#F69505"), PorterDuff.Mode.SRC_ATOP)
//
//            ratingBar.rating = 0F
            ratingBar.setOnRatingBarChangeListener(object : RatingBar.OnRatingBarChangeListener {
                override fun onRatingChanged(p0: RatingBar?, p1: Float, p2: Boolean) {
                     ratingvalue = p1.toInt()
                     if(p1 <=4)
                     {
                         skippp.visibility = View.VISIBLE
                         bottomcontainer.visibility = View.VISIBLE
                     }
                     else
                     {
                         skippp.visibility = View.GONE
                         bottomcontainer.visibility = View.GONE
                     }
                     //Toast.makeText(activity, "Given rating is: $p1", Toast.LENGTH_SHORT).show()
                }
            })

            submitt.setOnClickListener{

                if(ratingvalue>0)
                {
                    dataToSave.id =System.currentTimeMillis()
                    dataToSave.ratingvalue = ratingvalue
                    viewModel.insertFeedback(requireContext(), dataToSave)
                    ratingvalue = 0
                    dialog?.dismiss()
                    val dialogg = showdatasuccessDialog(requireContext())
                    dialogg.show()

                }
                else
                {
                    Toast.makeText(activity, "Please enter a Rating", Toast.LENGTH_SHORT).show()
                }
            }


        viewPager!!.offscreenPageLimit = 2
        Handler().postDelayed(Runnable { viewPager.adapter = adapter },1000)
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position
                when(tab.position)
                {
                    0 ->
                    {
                        submitt.visibility = View.VISIBLE
                        skippp.visibility = View.VISIBLE

                    }

                    1 ->
                    {
                        submitt.visibility = View.VISIBLE
                        skippp.visibility = View.VISIBLE

                    }

                    2 ->
                    {
                        skippp.visibility = View.INVISIBLE
                        submitt.visibility = View.VISIBLE

                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }

    override fun onStart() {
        super.onStart()
        val param = dialog?.window?.attributes
        val displayMetrics = getActivity()?.resources?.displayMetrics
        param?.width = displayMetrics?.widthPixels
      //  param?.height = (displayMetrics?.heightPixels?.minus(400))
        param?.height = ViewGroup.LayoutParams.WRAP_CONTENT;

        param?.gravity = Gravity.CENTER
        dialog?.window?.attributes = param
    }

}