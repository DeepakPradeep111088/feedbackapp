package com.mindorks.retrofit.coroutines.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.bestdocs.feedbackapp.R
import kotlinx.android.synthetic.main.comments_dialog.*
import kotlinx.android.synthetic.main.datainserted_dialog.*

class showdatasuccessDialog : Dialog {
    private var mContext: Context

    constructor(context: Context) : super(context) {
        mContext = context

    }
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setContentView(R.layout.datainserted_dialog)

        later.setOnClickListener(View.OnClickListener { dismiss() })

        ratenw.setOnClickListener(View.OnClickListener {
            Toast.makeText(mContext, "Sorry, unable to navigate to playstore", Toast.LENGTH_SHORT).show()
        })
    }

    public override fun onStart() {
        super.onStart()
        val param = this.window!!.attributes
        val displayMetrics =
            mContext.resources.displayMetrics
        param.width = displayMetrics.widthPixels
        param?.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        this.window!!.attributes = param
    }
}