package com.mindorks.retrofit.coroutines.utils

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}