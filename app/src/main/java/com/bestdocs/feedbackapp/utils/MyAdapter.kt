package com.mindorks.retrofit.coroutines.utils

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import com.bestdocs.feedbackapp.ui.main.view.AudioFragment
import com.bestdocs.feedbackapp.ui.main.view.OtherFragment
import com.bestdocs.feedbackapp.ui.main.view.VideoFragment


@Suppress("DEPRECATION")
internal class MyAdapter(
    fm: FragmentManager,
    var totalTabs: Int
) :
    FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                AudioFragment()
            }
            1 -> {
                VideoFragment()
            }
            2 -> {
                OtherFragment()
            }
            else -> getItem(position)
        }
    }
    override fun getCount(): Int {
        return totalTabs
    }
}