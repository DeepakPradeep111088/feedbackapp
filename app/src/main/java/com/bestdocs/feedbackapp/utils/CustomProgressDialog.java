package com.bestdocs.feedbackapp.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.WindowManager;

import com.bestdocs.feedbackapp.R;


/**
 * Created by Deepak Pradeep on 10/10/2018.
 */
public class CustomProgressDialog {
    private static ProgressDialog mLoadingDialog;
    private static ProgressDialog mLoadingMessageDialog;
    private static boolean isCancelled = false;


    public static void getLoadingIcon(final Context context, boolean cancelable, boolean disabledimflag) {
        try {
            isCancelled = false;
            hideLoadingIcon();
            mLoadingDialog = new ProgressDialog(context, ProgressDialog.THEME_HOLO_LIGHT);
            mLoadingDialog.setCancelable(cancelable);
            mLoadingDialog.show();
            mLoadingDialog.setContentView(R.layout.loading_dialog_white_wheel);
            if(disabledimflag) {
                mLoadingDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            }
            mLoadingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    isCancelled = true;
                }
            });
            //  try {
            //     mLoadingDialog.show();
        } catch (WindowManager.BadTokenException e) {

        }

    }
//    public static void getProgressMessage(final Context context, String message, boolean cancelable, boolean disabledimflag) {
//        try {
//            mLoadingMessageDialog = new ProgressDialog(context, ProgressDialog.THEME_HOLO_LIGHT);
//            mLoadingMessageDialog.setCancelable(cancelable);
//            mLoadingMessageDialog.setMessage(message);
//            mLoadingMessageDialog.show();
//            if(disabledimflag) {
//                mLoadingMessageDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//            }
//        } catch (WindowManager.BadTokenException e) {
//
//        }
//
//    }


//    public static void hideProgressMessage() {
//        try {
//            if (mLoadingMessageDialog != null && mLoadingMessageDialog.isShowing()) {
//                mLoadingMessageDialog.dismiss();
//            }
//        }catch ( IllegalArgumentException e) {
//
//        }
//
//    }

    public static boolean isVisible()
    {
        if (mLoadingDialog != null ) {
            return mLoadingDialog.isShowing();
        }
        return false;
    }

    public static void hideLoadingIcon() {
        try {
            if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
                mLoadingDialog.dismiss();
            }
        }catch ( IllegalArgumentException e) {

        }

    }
}
