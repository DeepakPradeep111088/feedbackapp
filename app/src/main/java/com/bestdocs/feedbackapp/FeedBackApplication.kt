package com.bestdocs.feedbackapp

import android.app.Application
import com.bestdocs.feedbackapp.di.apimodule
import com.bestdocs.feedbackapp.di.appModule
import com.bestdocs.feedbackapp.di.netmodule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
class FeedBackApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@FeedBackApplication)
            modules(appModule, apimodule, netmodule)
        }
    }
}