package com.bestdocs.feedbackapp.di

import com.bestdocs.feedbackapp.data.api.ApiService
import com.bestdocs.feedbackapp.data.repository.MainRepository
import com.bestdocs.feedbackapp.ui.main.viewmodel.MainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Deepak Pradeep on 9/12/20.
 */
val apimodule = module {
    fun provideapi(retrofit: Retrofit): ApiService
    {
        return retrofit.create(ApiService::class.java)
    }
    single { provideapi(get()) }
}


val netmodule = module{

    fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://connect.dev.bestdocapp.in/api/admin/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    single { getRetrofit() }
}


val appModule = module {
    single  { MainRepository(get()) }
    viewModel { MainViewModel(get()) }
}